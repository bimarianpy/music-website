## utility file for capturing api wrappers.
import re

from django.conf import settings
from .models import Artist

try:
    import pyechonest
except ImportError:
    raise ImproperlyConfigured("Echonest python library not installed")

from pyechonest import config, artist


class Echoutils(object):
    """ wrapper for interfacing echonest API """
    # config echonest with api key
    def __init__(self):
        config.ECHO_NEST_API_KEY = getattr(settings, 'EN_APIK')

    def getArtist(self, key="", exact=False):
        # lets return no results if the search string is blanked.
        if key.strip() == "":
            if exact == False or int(exact) == 0:
                return [], False
            else:
                return [], True

        # in case exact search return only one or zero results as per match
        try:
            res = artist.search(key.strip().lower())
        except EchoNestIOError:
            res = []

        if int(exact) == 1:
            result = []
            for each in res:
                # go and fetch the exact match.
                if re.search("^%s$" % key.strip().lower(), each.name.lower()):
                    result.append(each)
                    break
            return result, True

        else:
            return res, False

    def dbSyncArtist(self,artist_list):
        """ function to sync artist data to local database """
        if type(artist_list) is list:
            for each in artist_list:
                # get similar artists
                similar_list = each.similar
                # as its not a recursive loop we have to repeat ourselves
                similar_objects = []
                for similars in similar_list:
                    # first populate the similar artists
                    d = Artist.objects.get_or_create(artist_name=similars.name,
                                                 artist_id=similars.id)
                    if d[1] == True:
                        similar_objects.append(d[0])
                a = Artist.objects.get_or_create(artist_name=each.name,
                                             artist_id=each.id)
                if a[1] == True and similar_objects:
                    # now update the artist and its relations.
                    a[0].related_to.add(*similar_objects)