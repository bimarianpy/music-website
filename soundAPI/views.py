# Create your views here.
from django.template import loader, RequestContext, Context
from django.core.paginator import Paginator, PageNotAnInteger
from django.http import HttpResponse, HttpResponseServerError, \
    HttpResponseRedirect

from .forms import searchForm
from .utils import Echoutils


def search(request):
    """ search interface for artist similarity finding """
    if request.is_ajax():
        artist_name = request.GET.get('keyword')
        exact = request.GET.get('exact')
        if exact is None or artist_name is None:
            return HttpResponseServerError("Malformed data submission.")
        echo = Echoutils()
        #import pdb
        #pdb.set_trace()
        results = echo.getArtist(artist_name, exact)
        # sync results to database for now.
        echo.dbSyncArtist(results[0])
        request.session['search_result'] = results
        return HttpResponseRedirect('/search/results/')
    else:
        form = searchForm()
        context = RequestContext(request, {'form': form})
        template = loader.get_template('soundAPI/search.html')
        return HttpResponse(template.render(context))
        

def results(request):
    results = request.session.get('search_result')
    # start paginating the results.
    if type(results) is tuple:
        # print "start paginating the results"
        paginator = Paginator(results[0], 5)
        num = request.GET.get('page_num')
        try:
            res = paginator.page(num)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            res = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 999), deliver last page of results.
            res = paginator.page(paginator.num_pages)
        template = loader.get_template('soundAPI/resultslist.html')
        context = Context({
            'results': res,
            'exact' : results[1],
        })
        return HttpResponse(template.render(context))
    else:
        return HttpResponseServerError('API ERROR')
