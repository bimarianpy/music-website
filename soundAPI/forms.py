from django.forms import Form, CharField, ChoiceField

choice_list = (
    (0, 'all matches'),
    (1, 'exact match'),
    )

class searchForm(Form):
    artist_name = CharField(label="search keyword", max_length=50)
    exact = ChoiceField(label="search type", choices=choice_list)