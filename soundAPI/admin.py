from django.contrib.admin import site

from .models import Artist

site.register(Artist)
