from django.db import models


class Artist(models.Model):
    """Model class to record Artist name and ID from Echonest"""
    artist_name = models.CharField(max_length=200)
    artist_id = models.CharField(unique=True, max_length=50)
    # similar relationship is defined.
    related_to = models.ManyToManyField("self", null=True, blank=True,
                                        symmetrical=False)

    def __unicode__(self):
        return u'%s' % self.artist_name
