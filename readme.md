Purpose/Highlights of the demo(SoundAPI):

Code Sample Theme:
Sample application downloads Eco Nest artist information using API call,
stores in local database.

APP name: SoundAPI
Project name: POC

SETTINGS BEFORE START:

1. in project settings file (poc/settings.py) do the following:

    a) define a database (currently sqlite is set as a database)

    b) add echonest api key for example, we have set a demo key like:

    EN_APIK = "[my api key from eco nest]"

    c) in installed apps, add SoundAPI app.

2. define the application urls in root url configuration:

    a)url(r'^$', 'soundAPI.views.search', name='search'),

    b)url(r'search/results/', 'soundAPI.views.results', name='results'),


Code Highlights:

1. To interface Eco Nest api pyeconest officially recognized library used.

2. Show use of django framework.

   a) Show how the same view code can handle different http requests ( GET / AJAX)

   b) Django Templates based on contexts.

   c) Use of utility functions (in utils.py) for code reuse.

   d) Use of Jquery for ajax calls.

   e) Twitter's Bootstrap framework based UI.

   f) Mobile friendly One-Page application.


Known Limitations of Demo Code:

1. Scripts / Css is Inline ( for quick evaluation) should be ideally as external files.
2. Corner use-cases(events) are not investigated.
3. Basic usability testing is done, no unit tests written.
4. Relational model defined in the app(models.py) to show the Artists, similar artists.
    this is not extensive as it is a limited feature demo.

development platform:

1. Virtualenv
2. Django 1.5
3. Jquery 2.0
4. twitter's bootstrap library
5. Python 2.6.6
6. Centos 6.2
7. Usability testing on Firefox 18.x / Chrome 24.0.x
8. database: sqlite

SHOWCASE:

step 1. navigate to url http://dev_webserver_hostname:port/

We will be presented with a search interface. In the search interface key in

artist name. A spinner is shown while results are being fetched in the backend.

Results are shown next in a tabular form.

step 2. Change the search match type (Enhancement from the default api feature).

We can either choose to fetch 'all matches' or 'exact matches' for the artist name.

In case of 'exact matches' only one result or no result is returned, narrowing down

the scope of search.

step 3. In case of more than 5 search results, we can navigate between the

result is split between pages(theoretically). Actually we remain in the same

page and navigate between the results using "previous" and "next" links.

This is very contemporary and being targetted for mobile users who have the

convenience of accessing information from the same place.